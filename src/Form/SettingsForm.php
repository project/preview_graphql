<?php

namespace Drupal\preview_graphql\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\preview_graphql\Event\CarrierCallbackOptionEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($config_factory);
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('config.factory'),
      // Load the service required to construct this class.
      $container->get('event_dispatcher')

    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'preview_graphql.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('preview_graphql.settings');
    $form = parent::buildForm($form, $form_state);

    $form['url_front'] = [
      '#type' => 'url',
      '#default_value' => $config->get('url_front'),
      '#required' => TRUE,
      '#title' => $this->t('Front url'),
    ];
    $form['uri_front'] = [
      '#type' => 'textfield',
      '#default_value' => $config->get('uri_front'),
      '#required' => TRUE,
      '#title' => $this->t('Front path'),
    ];

    $doc_url = 'https://www.drupal.org/docs/8/modules/preview-graphql';
    $options = [
      'key' => $this->t('Use your own url parameter key to transmit data.'),
    ];

    $event = new CarrierCallbackOptionEvent($options);
    $this->eventDispatcher->dispatch(CarrierCallbackOptionEvent::EVENT_NAME, $event);

    $form['carrier_callback'] = [
      '#type' => 'radios',
      '#options' => $event->getOptions(),
      '#required' => TRUE,
      '#title' => $this->t('Carrier data send'),
      '#description' => $this->t("Choose what carrier the data. If you don't know the difference, see the <a href=\"@doc\">documentation</a>", ['@doc' => $doc_url]),
      '#default_value' => !empty($config->get('carrier_callback')) ? $config->get('carrier_callback') : 'key',
    ];

    $form['carrier_callback_key'] = [
      '#type' => 'machine_name',
      '#default_value' => !empty($config->get('carrier_callback_key')) ? $config->get('carrier_callback_key') : 'key',
      '#states' => [
        'visible' => [
          ':input[name="carrier_callback"]' => ['value' => 'key'],
        ],
        'required' => [
          ':input[name="carrier_callback"]' => ['value' => 'key'],
        ],
      ],
      '#title' => $this->t('The key name url for redirection.'),
      '#description' => $this->t('Enter the url parameter name you want for the redirection to front and come back to drupal. Used into both the redirection.'),
    ];
    $options = [
      'get' => $this->t('Use GET callback'),
      'post' => $this->t('Use POST callback'),
    ];
    $form['method'] = [
      '#type' => 'radios',
      '#options' => $options,
      '#required' => TRUE,
      '#title' => $this->t('Method data send'),
      '#description' => $this->t("Choose what method you want to use to send the data. If you don't know the difference, see the <a href=\"@doc\">documentation</a>", ['@doc' => $doc_url]),
      '#default_value' => $config->get('method'),
    ];

    $form['post_url_key'] = [
      '#type' => 'machine_name',
      '#default_value' => !empty($config->get('post_url_key')) ? $config->get('post_url_key') : 'redirect_url',
      '#states' => [
        'visible' => [
          ':input[name="method"]' => ['value' => 'post'],
        ],
        'required' => [
          ':input[name="method"]' => ['value' => 'post'],
        ],
      ],
      '#title' => $this->t('The key name for callback url POST'),
      '#description' => $this->t('Enter the url parameter name you want for redirection to front when the front answer to Drupal.'),
    ];
    $form['mutation_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable mutation'),
      '#description' => $this->t('Enable callback save and cancel mutation into graphQL. Use this option with precaution, depending what permission you set on graphql endpoint.'),
      '#default_value' => $config->get('mutation_enable'),
    ];
    $form['graphql'] = [
      '#type' => 'details',
      '#tree' => FALSE,
      '#title' => $this->t('Default GraphQL query'),
    ];
    $form['graphql']['query_gql'] = [
      '#type' => 'textarea',
      '#default_value' => $config->get('query_gql'),
      '#required' => TRUE,
      '#rows' => 25,
      '#title' => $this->t('Default query graphql'),
      '#description' => $this->t('Put here the content of your query as you call the endpoint to get your data. Do not include the first parent field, only the content.For more information see <a href="@doc">documentation</a>', ['@doc' => $doc_url]),
    ];
    $form['graphql']['fragment_gql'] = [
      '#type' => 'textarea',
      '#rows' => 50,
      '#default_value' => $config->get('fragment_gql'),
      '#required' => TRUE,
      '#title' => $this->t('Default fragment graphql'),
      '#description' => $this->t('Put here the same query as you call the endpoint to get your data.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('preview_graphql.settings')
      ->set('url_front', $form_state->getValue('url_front'))
      ->set('uri_front', $form_state->getValue('uri_front'))
      ->set('carrier_callback', $form_state->getValue('carrier_callback'))
      ->set('carrier_callback_key', $form_state->getValue('carrier_callback_key'))
      ->set('method', $form_state->getValue('method'))
      ->set('post_url_key', $form_state->getValue('post_url_key'))
      ->set('query_gql', $form_state->getValue('query_gql'))
      ->set('fragment_gql', $form_state->getValue('fragment_gql'))
      ->set('mutation_enable', $form_state->getValue('mutation_enable'))
      ->save();
  }

}
