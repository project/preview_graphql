<?php

namespace Drupal\preview_graphql\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\graphql\Utility\StringHelper;
use Drupal\preview_graphql\Entity\PreviewGraphQL;
use Drupal\preview_graphql\Services\PreviewGraphQlManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PreviewGraphQLForm.
 */
class PreviewGraphQLForm extends EntityForm {

  /**
   * A entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A bundle manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * A preview graphQL manager instance.
   *
   * @var \Drupal\preview_graphql\Services\PreviewGraphQlManager
   */
  protected $previewGraphQlManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_bundle_info, PreviewGraphQlManager $preview_graphql_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityBundleInfo = $entity_bundle_info;
    $this->previewGraphQlManager = $preview_graphql_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('preview_graphql.manager')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /* @var  $preview_graphql \Drupal\preview_graphql\Entity\PreviewGraphQL */
    $preview_graphql = $this->entity;
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $preview_graphql->id(),
      '#machine_name' => [
        'exists' => '\Drupal\preview_graphql\Entity\PreviewGraphQL::load',
      ],
      '#disabled' => !$preview_graphql->isNew(),
    ];
    // Build entity type.
    $entity_type_definitions = $this->entityTypeManager->getDefinitions();;
    $type_options = [];
    foreach ($entity_type_definitions as $type => $type_info) {
      $type_options[$type] = $type_info->getLabel()->render();
    }
    if (!empty($type_options)) {
      asort($type_options);
    }
    $default_entity_type = $this->getDefaultValue($form_state, $preview_graphql, 'entity_type');
    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Target entity type'),
      '#options' => $type_options,
      '#default_value' => $default_entity_type,
      '#required' => TRUE,
      '#multiple' => FALSE,
      '#ajax' => [
        'callback' => [$this, 'ajaxBundle'],
        'wrapper' => 'ajax-bundle',
        'method' => 'replaceWith',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];
    $form['ajax_bundle'] = [
      '#type' => 'container',
      '#tree' => FALSE,
      '#prefix' => '<div id="ajax-bundle">',
      '#suffix' => '</div>',
    ];
    if (empty($default_entity_type)) {
      return $form;
    }
    $entity_type_definition = $this->entityTypeManager->getDefinition($default_entity_type);
    $default_bundle = NULL;
    // Content entity.
    if ($entity_type_definition->getGroup() != 'configuration') {
      $options = [];
      foreach ($this->entityBundleInfo->getBundleInfo($default_entity_type) as $bundle_name => $label) {
        $options[$bundle_name] = $label['label'];
      }
      if (!empty($options)) {

        $default_bundle = $this->getDefaultValue($form_state, $preview_graphql, 'bundle');
        asort($options);
        // Do not set parent cause element has #name proprieties.
        $form['ajax_bundle']['bundle'] = [
          '#type' => 'select',
          '#title' => $this->t('Target entity bundle'),
          '#description' => $this->t('Select bundle for which you want a preview. Leave empty to select all.'),
          '#options' => $options,
          '#multiple' => TRUE,
          '#default_value' => $default_bundle,
          '#states' => [
            'visible' => [
              ':input[name=entity_type]' => ['filled' => TRUE],
            ],
          ],
          '#ajax' => [
            'callback' => [$this, 'ajaxBundle'],
            'wrapper' => 'ajax-bundle',
            'method' => 'replaceWith',
            'progress' => [
              'type' => 'throbber',
              'message' => NULL,
            ],
          ],
        ];

      }
    }
    $type_has_previews = ['node'];
    foreach ($type_has_previews as $type_has_preview) {
      if ($default_entity_type == $type_has_preview) {
        $form['ajax_bundle']['disable_preview_btn'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Hide preview button'),
          '#description' => $this->t('If check, the preview button will be hide in :type entity form.', [':type' => $type_has_preview]),
          '#default_value' => $this->getDefaultValue($form_state, $preview_graphql, 'disable_preview_btn'),
        ];
      }
    }
    $options = ['entity_type' => $this->t('Your query will receive %entity_type type', ['%entity_type' => StringHelper::camelCase($default_entity_type)])];
    if (!empty($default_bundle)) {
      $string_bundle = [];
      foreach ($default_bundle as $def_bundle) {
        $string_bundle[] = StringHelper::camelCase($default_entity_type . ' ' . $def_bundle);
      }
      $options['bundle'] = $this->t('Your query will receive the specific graphQL type by bundle (%bundle).', ['%bundle' => implode(',', $string_bundle)]);
    }
    $default_deriver = $this->getDefaultValue($form_state, $preview_graphql, 'deriver');
    $form['ajax_bundle']['deriver'] = [
      '#type' => 'radios',
      '#options' => $options,
      '#required' => TRUE,
      '#title' => $this->t('GraphQL type received.'),
      '#description' => $this->t('Choose what GraphQL type your query will receive.'),
      '#default_value' => $default_deriver,
    ];
    $override_details = $preview_graphql->get('override');
    $form['ajax_bundle']['override'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#title' => $this->t('Override global settings'),
    ];
    $form['ajax_bundle']['override']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable override'),
      '#description' => $this->t('If not check, default global settings will be take.'),
      '#default_value' => !empty($override_details['enable']) ? $override_details['enable'] : FALSE,
    ];
    $form['ajax_bundle']['override']['uri_front'] = [
      '#type' => 'textfield',
      '#default_value' => !empty($override_details['uri_front']) ? $override_details['uri_front'] : '',
      '#title' => $this->t('Front path'),
      '#states' => [
        'visible' => [
          ':input[name="override[enable]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="override[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $link = Link::fromTextAndUrl(t('documentation'), Url::fromUri('https://www.drupal.org/docs/8/modules/preview-graphql'));
    $form['ajax_bundle']['override']['query_gql'] = [
      '#type' => 'textarea',
      '#default_value' => !empty($override_details['query_gql']) ? $override_details['query_gql'] : '',
      '#title' => $this->t('Default query graphql'),
      '#description' => $this->t('Put here the content of your query as you call the endpoint to get your data. Do not include the first parent field, only the content.For more information see @doc', ['@doc' => $link]),
      '#rows' => 25,
      '#states' => [
        'visible' => [
          ':input[name="override[enable]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="override[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['ajax_bundle']['override']['fragment_gql'] = [
      '#type' => 'textarea',
      '#default_value' => !empty($override_details['fragment_gql']) ? $override_details['fragment_gql'] : '',
      '#title' => $this->t('Default fragment graphql'),
      '#description' => $this->t('Put here the same query as you call the endpoint to get your data.'),
      '#rows' => 50,
      '#states' => [
        'visible' => [
          ':input[name="override[enable]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="override[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $entity = $form_state->getFormObject()->getEntity();

    $preview_graphql = $this->previewGraphQlManager->getPreviewGraphQlEntity($entity);
    if (!empty($preview_graphql) && ($preview_graphql->id() != $entity->id())) {
      $form_state->setErrorByName('entity_type', $this->t('There is already configuration for this entity, or bundle'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $preview_graphql = $this->entity;
    $status = $preview_graphql->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label Preview graph QL entity.', [
            '%label' => $preview_graphql->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label Preview graph QL entity.', [
            '%label' => $preview_graphql->label(),
          ]));
    }

    $form_state->setRedirectUrl($preview_graphql->toUrl('collection'));
  }

  /**
   * Ajax callback target bundle.
   *
   * @inheritdoc
   */
  public function ajaxBundle(array &$form, FormStateInterface $form_state) {
    return $form['ajax_bundle'];
  }

  /**
   * Return default value for element form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\preview_graphql\Entity\PreviewGraphQL $preview_graphql
   *   The current preview graphql entity.
   * @param string $key_value
   *   The field key searched.
   *
   * @return mixed|null
   *   The value of field.
   */
  private function getDefaultValue(FormStateInterface $form_state, PreviewGraphQL $preview_graphql, $key_value) {
    $value = $form_state->getValue($key_value);
    if (!empty($value)) {
      return $value;
    }
    $value = $preview_graphql->get($key_value);

    return $value;
  }

}
