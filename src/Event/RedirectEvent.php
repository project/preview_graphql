<?php

namespace Drupal\preview_graphql\Event;

use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Event that is fired before graphql preview set the redirect response.
 *
 * @see \Drupal\preview_graphql\Services\\PreviewGraphQlManager::sendDataFront().
 */
class RedirectEvent extends PreviewGraphQlEventBase {

  const EVENT_NAME = 'preview_graphql.redirect';

  /**
   * The front redirection url.
   *
   * @var string
   */
  public $urlParameterValue;

  /**
   * The redirect response.
   *
   * @var \Drupal\Core\Routing\TrustedRedirectResponse
   */
  protected $response;

  /**
   * RedirectEvent constructor.
   *
   * @param string $carrier_callback
   *   The type carrier callback key.
   * @param \Drupal\Core\Routing\TrustedRedirectResponse $response
   *   The redirect response.
   * @param string $url_parameter_value
   *   The front url to redirect.
   */
  public function __construct($carrier_callback, TrustedRedirectResponse $response, $url_parameter_value) {
    parent::__construct($carrier_callback);
    $this->carrierCallback = $carrier_callback;
    $this->response = $response;
    $this->urlParameterValue = $url_parameter_value;
  }

  /**
   * Set the redirect response.
   *
   * @param \Drupal\Core\Routing\TrustedRedirectResponse $response
   *   The redirect response.
   */
  public function setResponse(TrustedRedirectResponse $response) {
    $this->response = $response;
  }

  /**
   * Get the redirect response.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   The redirect response.
   */
  public function getResponse() {
    return $this->response;
  }

}
