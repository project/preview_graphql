<?php

namespace Drupal\preview_graphql\Event;

use Drupal\graphql\GraphQL\Execution\ResolveContext;

/**
 * Event that is fired when graphql preview is processing.
 *
 * @see \Drupal\preview_graphql\Plugin\GraphQL\Fields\PreviewGraphQLField.
 */
class ContextResolverCacheEvent extends PreviewGraphQlEventBase {

  const EVENT_NAME = 'preview_graphql.context_resolver_cache';

  /**
   * GraphQL context resolver.
   *
   * @var \Drupal\graphql\GraphQL\Execution\ResolveContext
   */
  protected $context;

  /**
   * ContextResolverCacheEvent constructor.
   *
   * @param string $carrier_callback
   *   The type carrier callback key.
   * @param \Drupal\graphql\GraphQL\Execution\ResolveContext $context
   *   The graphQL context resolver.
   */
  public function __construct($carrier_callback, ResolveContext $context) {
    parent::__construct($carrier_callback);
    $this->context = $context;
  }

  /**
   * Set the graphQL context resolver.
   *
   * @param \Drupal\graphql\GraphQL\Execution\ResolveContext $context
   *   GraphQL context resolver.
   */
  public function setContext(ResolveContext $context) {
    $this->context = $context;
  }

  /**
   * Get the graphQL context resolver.
   *
   * @return \Drupal\graphql\GraphQL\Execution\ResolveContext
   *   GraphQL context resolver.
   */
  public function getContext() {
    return $this->context;
  }

}
