<?php

namespace Drupal\preview_graphql\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event that is fired when settings of carrier callback element is building.
 *
 * @see \Drupal\preview_graphql\Form\SettingsForm.
 */
class CarrierCallbackOptionEvent extends Event {

  const EVENT_NAME = 'preview_graphql.carrier_callback_option';

  protected $options = [];

  /**
   * CarrierCallbackOptionEvent constructor.
   *
   * @param array $options
   *   Options to build radios into global settings.
   */
  public function __construct(array $options = []) {
    $this->options = $options;
  }

  /**
   * Set the options carrier callback.
   *
   * @param array $options
   *   Options to build radios into global settings.
   */
  public function setOptions(array $options) {
    $this->options = $options;
  }

  /**
   * Add one option to carrier callback.
   *
   * @param array $option
   *   Options to build radios into global settings.
   */
  public function addOption(array $option) {
    $this->options = array_merge($this->options, $option);
  }

  /**
   * Set one option to carrier callback.
   *
   * @param string $key_option
   *   The key name of the option.
   * @param string $label_option
   *   The label of the option.
   */
  public function setOption($key_option, $label_option) {
    $this->options[$key_option] = $label_option;
  }

  /**
   * Return the options carrier callback.
   *
   * @return array
   *   Options to build radios into global settings.
   */
  public function getOptions() {
    return $this->options;
  }

}
