<?php

namespace Drupal\preview_graphql\Event;

/**
 * Event that is fired when preview graphql try to retrieve the cache id.
 *
 * @see \Drupal\preview_graphql\Services\\PreviewGraphQlBackend::getCid().
 */
class GetCidEvent extends PreviewGraphQlEventBase {

  const EVENT_NAME = 'preview_graphql.get_cid';

  /**
   * The cache id of preview.
   *
   * @var string
   */
  protected $cid;

  /**
   * Set the cache id of preview.
   *
   * @param string $cid
   *   The cache id of preview.
   */
  public function setCid($cid) {
    $this->cid = $cid;
  }

  /**
   * Get the cache id of preview.
   *
   * @return string
   *   The cache id of preview.
   */
  public function getCid() {
    return $this->cid;
  }

}
