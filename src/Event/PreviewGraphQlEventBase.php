<?php

namespace Drupal\preview_graphql\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event preview graphql Base.
 */
abstract class PreviewGraphQlEventBase extends Event {

  /**
   * The type carrier callback key.
   *
   * @var string
   */
  public $carrierCallback;

  /**
   * PreviewGraphQlEventBase constructor.
   *
   * @param string $carrier_callback
   *   The type carrier callback key.
   */
  public function __construct($carrier_callback) {
    $this->carrierCallback = $carrier_callback;
  }

}
