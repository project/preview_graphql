<?php

namespace Drupal\preview_graphql\Event;

/**
 * Event that is fired when graphql preview needs to generate the carrier.
 *
 * @see \Drupal\preview_graphql\Services\\PreviewGraphQlManager::sendDataFront().
 */
class GenerateKeyEvent extends PreviewGraphQlEventBase {

  const EVENT_NAME = 'preview_graphql.generate_key';

  /**
   * The cache id of preview.
   *
   * @var string
   */
  public $cid;

  /**
   * The value of carrier.
   *
   * @var string
   */
  protected $carrierValue;

  /**
   * GenerateKeyEvent constructor.
   *
   * @param string $carrier_callback
   *   The type carrier callback key.
   * @param string $cid
   *   The cache id.
   */
  public function __construct($carrier_callback, $cid) {
    parent::__construct($carrier_callback);
    $this->carrierCallback = $carrier_callback;
    $this->cid = $cid;
    $this->carrierValue = $cid;
  }

  /**
   * Set the value of carrier.
   *
   * @param string $carrier_value
   *   The value of carrier.
   */
  public function setValueKey($carrier_value) {
    $this->carrierValue = $carrier_value;
  }

  /**
   * Get the value of carrier.
   *
   * @return string
   *   The value of carrier.
   */
  public function getValueKey() {
    return $this->carrierValue;
  }

}
