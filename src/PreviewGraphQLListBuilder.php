<?php

namespace Drupal\preview_graphql;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Preview graph QL entity entities.
 */
class PreviewGraphQLListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Machine name');
    $header['entity_type'] = $this->t('Entity Type');
    $header['bundle'] = $this->t('Bundle');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['entity_type'] = $entity->get('entity_type');
    $row['bundle'] = implode(',', $entity->get('bundle'));
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

}
