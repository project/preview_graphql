<?php

namespace Drupal\preview_graphql\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\preview_graphql\Event\GetCidEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PreviewGraphQLBackend.
 *
 * @package Drupal\preview_graphql\Services
 */
class PreviewGraphQlBackend {


  /**
   * The cache id where the entity and graphql result are saved.
   *
   * @var string
   */
  protected $cid;

  /**
   * The request instance.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;


  /**
   * The cache factory instance.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The private temp store service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $privateTempStore;


  /**
   * A messenger instance.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * A entity logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * A cache tag invalidator instance.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagInvalidator;

  protected $eventDispatcher;

  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack, ConfigFactoryInterface $config_factory, CacheBackendInterface $cache_backend, PrivateTempStoreFactory $private_temp_store, EventDispatcherInterface $event_dispatcher, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger_factory, CacheTagsInvalidatorInterface $cache_tag_invalidator) {
    $this->request = $request_stack->getCurrentRequest();
    $this->cacheBackend = $cache_backend;
    $this->config = $config_factory->get('preview_graphql.settings');
    $this->eventDispatcher = $event_dispatcher;
    $this->privateTempStore = $private_temp_store;
    $this->messenger = $messenger;
    $this->logger = $logger_factory->get('preview_graphql');
    $this->cacheTagInvalidator = $cache_tag_invalidator;
  }

  /**
   * Return the request manager.
   *
   * @return \Symfony\Component\HttpFoundation\Request|null
   *   An instance of request manager.
   */
  public function getRequest() {
    return $this->request;
  }

  /**
   * Get service messenger.
   *
   * @return \Drupal\Core\Messenger\MessengerInterface
   *   An instance of messenger service.
   */
  public function getMessenger() {

    return $this->messenger;
  }

  /**
   * Get service logger.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   An instance of logger service.
   */
  public function getLogger() {

    return $this->logger;
  }

  /**
   * Get service config preview graphql.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The config preview graphql settings.
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * Get service event dispatcher.
   *
   * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
   *   An instance of event dispatcher.
   */
  public function getEventDispatcher() {
    return $this->eventDispatcher;
  }

  /**
   * Return the cache backend factory.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   An instance of cache backend factory.
   */
  public function getCacheBackend() {
    return $this->cacheBackend;
  }

  /**
   * Get service cache tag invalidator.
   *
   * @return \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   *   An instance of cache tag invalidator.
   */
  public function getCacheTagInvalidator() {
    return $this->cacheTagInvalidator;
  }

  /**
   * Save into temp store the form state object.
   *
   * @param string $temp_id
   *   The id of temp store.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object to saved.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function saveTempStore($temp_id, FormStateInterface $form_state) {
    $temp_store = $this->privateTempStore->get('preview_graphql');
    $temp_store->set($temp_id, $form_state);
  }

  /**
   * Delete into temp store.
   *
   * @param string $temp_id
   *   The id of temp store.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function deleteTempStore($temp_id) {
    $temp_store = $this->privateTempStore->get('preview_graphql');
    $temp_store->delete($temp_id);
  }

  /**
   * Get the current cid.
   *
   * @return string
   *   The cid value
   */
  public function getCid() {
    $carrier_callback = $this->config->get('carrier_callback');
    if ($carrier_callback != 'key') {
      $event = new GetCidEvent($carrier_callback);
      $this->eventDispatcher->dispatch(GetCidEvent::EVENT_NAME, $event);
      $this->cid = $event->getCid();
    }
    else {
      $this->cid = $this->getCidByRequest($this->config->get('carrier_callback_key'));
    }
    return $this->cid;
  }

  /**
   * Get Cid by the request url.
   *
   * @param string $carrier_callback_key
   *   The string name for the carrier callback.
   *
   * @return string
   *   The cid value
   */
  protected function getCidByRequest($carrier_callback_key) {
    if (empty($this->cid)) {
      $this->cid = $this->request->query->get($carrier_callback_key);
    }
    return $this->cid;
  }

}
