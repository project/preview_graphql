<?php

namespace Drupal\preview_graphql\Services;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Uuid\Php;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\graphql\Utility\StringHelper;
use Drupal\preview_graphql\Entity\PreviewGraphQL;
use Drupal\preview_graphql\Event\RedirectEvent;
use GraphQL\Server\OperationParams;
use Drupal\graphql\GraphQL\Execution\QueryProcessor;
use Drupal\graphql\GraphQL\Execution\QueryResult;
use Drupal\preview_graphql\Event\GenerateKeyEvent;
use GuzzleHttp\Client;
use Exception;

/**
 * Class PreviewGraphQLManager.
 *
 * @package Drupal\preview_graphql\Service
 */
class PreviewGraphQlManager {

  use StringTranslationTrait;

  /**
   * A entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * The preview graphql backend service.
   *
   * @var \Drupal\preview_graphql\Services\PreviewGraphQlBackend
   */
  protected $previewGraphQlBackend;


  /**
   * The graphql query processor service.
   *
   * @var \Drupal\graphql\GraphQL\Execution\QueryProcessor
   */
  protected $queryProcessor;

  /**
   * The date time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $dateTime;

  /**
   * The uuid service.
   *
   * @var \Drupal\Component\Uuid\Php
   */
  protected $uuid;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Prefix cache tag name.
   */
  const PREFIX_CACHE = 'pgql_';

  // Max 24h.
  const DEFAULT_TIME_CACHE = 86400;

  /**
   * Contains the entity and correspondance preview graphql entity.
   *
   * @var array
   */
  protected $currentPreviewGraphqlEntity;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, PreviewGraphQlBackend $preview_graphql_backend, QueryProcessor $query_processor, TimeInterface $date_time, Php $uuid, Client $http_client) {
    $this->entityTypeManager = $entity_type_manager;
    $this->previewGraphQlBackend = $preview_graphql_backend;
    $this->queryProcessor = $query_processor;
    $this->dateTime = $date_time;
    $this->uuid = $uuid;
    $this->httpClient = $http_client;
    $this->currentPreviewGraphqlEntity = [
      'preview_entity' => NULL,
      'entity' => NULL,
    ];
  }

  /**
   * Return the preview graphql backend service.
   *
   * @return \Drupal\preview_graphql\Services\PreviewGraphQlBackend
   *   The preview graphql backend service.
   */
  public function getPreviewGraphQlBackend() {
    return $this->previewGraphQlBackend;
  }

  /**
   * Fill the form entity with the value before the preview.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current data form.
   *
   * @return bool
   *   False if error.
   */
  public function fillEntityForm(FormStateInterface $form_state) {
    $form_state_preview = $this->getCacheDataValue('form_state');
    if (empty($form_state_preview) || !($form_state_preview instanceof FormStateInterface)) {
      return FALSE;
    }

    if (!$form_state->isRebuilding()) {
      /* @var $form_state_preview \Drupal\Core\Form\FormStateInterface */
      $form_state->setStorage($form_state_preview->getStorage());
      $form_state->setUserInput($form_state_preview->getUserInput());

      // Rebuild the form.
      $form_state->setRebuild();

      // The combination of having user input and rebuilding the form means
      // that it will attempt to cache the form state which will fail if it is
      // a GET request.
      $form_state->setRequestMethod('POST');

      $entity = $form_state_preview->getFormObject()->getEntity();
      $entity->in_preview = NULL;
      $form_state->getFormObject()->setEntity($entity);
      $form_state->set('has_been_previewed', TRUE);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get preview graphql entity by entity type and/or bundle.
   *
   * @param string $entity_type
   *   The entity type of preview graphql entity.
   * @param string $bundle
   *   The entity bundle of preview graphql entity.
   * @param bool $load
   *   If true, load all preview graphql entity.
   *
   * @return array|\Drupal\preview_graphql\Entity\PreviewGraphQL[]|int
   *   The ids of preview graph ql entity or loaded entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPreviewGraphQlEntityByTypeBundle($entity_type, $bundle, $load = FALSE) {
    $query = $this->entityTypeManager
      ->getStorage('preview_graphql')->getQuery();

    $query->condition('entity_type', $entity_type, '=');
    // @todo change this part of code when core fixed.
    if (!empty($bundle)) {
      // @see https://www.drupal.org/project/drupal/issues/3053941
      // $query->condition('bundle', [$bundle], 'IN');
    }
    else {
      $query->condition('bundle', NULL, 'IS NULL');
    }
    $ids = $query->execute();
    // @todo change this part of code when core fixed.
    if (!empty($ids) && (!empty($bundle) || $load = TRUE)) {

      $entities = $this->entityTypeManager
        ->getStorage('preview_graphql')->loadMultiple($ids);
      // Filter per bundle.
      if (!empty($bundle)) {
        foreach ($entities as $entity_id => $preview_graphql_entity) {
          if (!in_array($bundle, $preview_graphql_entity->get('bundle'), TRUE)) {
            unset($entities[$entity_id]);
          }
        }
      }

      if ($load) {
        return $entities;
      }
      return array_keys($entities);
    }
    return $ids;
  }

  /**
   * Load all preview graphql entity and merge deriver type.
   *
   * @return array
   *   The merged of deriver type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDeriverType() {

    $deriver_type = ['entity_type' => [], 'bundle' => []];
    /* @var  $previews_graphqls \Drupal\preview_graphql\Entity\PreviewGraphQL[] */
    $previews_graphqls = $this->entityTypeManager->getStorage('preview_graphql')
      ->loadMultiple();
    foreach ($previews_graphqls as $previews_graphql) {
      if ($previews_graphql->get('deriver') == 'bundle' && !empty($previews_graphql->get('bundle'))) {
        foreach ($previews_graphql->get('bundle') as $bundle) {
          $deriver_type['bundle'][$previews_graphql->get('entity_type')][$bundle] = $bundle;
        }
        continue;
      }
      $deriver_type['entity_type'][$previews_graphql->get('entity_type')] = $previews_graphql->get('entity_type');
    }
    return $deriver_type;
  }

  /**
   * Return the preview graphql entity corresponding of an drupal entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which we search preview graphql entity.
   *
   * @return bool|\Drupal\preview_graphql\Entity\PreviewGraphQL
   *   False ifno preview graphql entity found, else the entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPreviewGraphQlEntity(EntityInterface $entity) {
    if (!empty($this->currentPreviewGraphqlEntity['entity']) && !empty($this->currentPreviewGraphqlEntity['preview_entity']) && $this->currentPreviewGraphqlEntity['entity']->uuid() == $entity->uuid()) {
      return $this->currentPreviewGraphqlEntity['preview_entity'];
    }
    $preview_graphql_entity = $this->getPreviewGraphQlEntityByTypeBundle($entity->getEntityTypeId(), $entity->bundle(), TRUE);
    if (empty($preview_graphql_entity)) {
      $preview_graphql_entity = $this->getPreviewGraphQlEntityByTypeBundle($entity->getEntityTypeId(), '', TRUE);
    }
    $this->currentPreviewGraphqlEntity['preview_entity'] = !empty($preview_graphql_entity) ? reset($preview_graphql_entity) : FALSE;
    return $this->currentPreviewGraphqlEntity['preview_entity'];
  }

  /**
   * Build the graphql query for entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to build query.
   *
   * @return bool|string
   *   False if error, else the graphql query.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildQueryByEntity(EntityInterface $entity) {
    $preview_graphql_entity = $this->getPreviewGraphQlEntity($entity);

    if (empty($preview_graphql_entity)) {
      $message = $this->t('No preview graphql entity found for this entity.');
      $this->previewGraphQlBackend->getMessenger()->addError($message);
      $this->previewGraphQlBackend->getLogger()->error($message);
      return FALSE;
    }
    $deriver = $preview_graphql_entity->get('deriver');

    // Init query name depend what query expect.
    $query_name = StringHelper::propCase('Preview_' . $entity->getEntityTypeId() . '_' . $entity->bundle());
    if ($deriver == 'entity_type') {
      $query_name = StringHelper::propCase('Preview_' . $entity->getEntityTypeId());
    }
    // Insert args into query gql.
    $query_name .= '(entity_type:"' . $entity->getEntityTypeId() . '",uuid:"' . $entity->uuid() . '")';
    $query_gql = $this->getSetting($preview_graphql_entity, 'query_gql');
    $fragment_gql = $this->getSetting($preview_graphql_entity, 'fragment_gql');
    // Insert query name, query gql, and fragment.
    $query = '{' . $query_name . '{' . $query_gql . '}}' . $fragment_gql;
    return $query;
  }

  /**
   * Process the entity query into graphQL engine.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to process.
   *
   * @return bool|\Drupal\graphql\GraphQL\Execution\QueryResult
   *   False if error, else the query result of graph ql.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function processQueryEntity(EntityInterface $entity) {
    // Process the graphql query.
    $query = $this->buildQueryByEntity($entity);
    // @todo get it dynamically.
    $schema = 'default:default';
    $operation = OperationParams::create(['query' => $query]);
    /* @var  $result \Drupal\graphql\GraphQL\Execution\QueryResult */
    $result = $this->queryProcessor->processQuery($schema, $operation);
    // Graphql Errors.
    if (!empty($result->errors)) {
      /* @var  $errors \GraphQL\Error\Error */
      foreach ($result->errors as $errors) {
        $message = $errors->getMessage();
        $this->previewGraphQlBackend->getMessenger()->addError($message);
        $this->previewGraphQlBackend->getLogger()->error($message);
        // Remove from temp store.
        $this->previewGraphQlBackend->deleteTempStore($entity->getEntityTypeId() . '_' . $entity->uuid());
        return FALSE;
      }
    }

    return $result;
  }

  /**
   * Get a settings by an entity (get the corresponding previewGQL entity).
   *
   * @param string $key
   *   The key name config.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The entity doing the preview.
   *
   * @return array|mixed|string|null
   *   The value of settings.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSettingByEntity($key, EntityInterface $entity = NULL) {
    if (empty($entity)) {
      $this->previewGraphQlBackend->getConfig()->get($key);
    }
    $preview_graphql_entity = $this->getPreviewGraphQlEntity($entity);
    if (empty($preview_graphql_entity)) {
      return $this->previewGraphQlBackend->getConfig()->get($key);
    }
    return $this->getSetting($preview_graphql_entity, $key);
  }

  /**
   * Get settings of the preview graphql entity.
   *
   * Also return the default settings if not override.
   *
   * @param \Drupal\preview_graphql\Entity\PreviewGraphQL $preview_graph_ql
   *   The preview graphql entity.
   * @param string $key
   *   The config name.
   *
   * @return string|null
   *   The config value.
   */
  public function getSetting(PreviewGraphQL $preview_graph_ql, $key) {
    $override = $preview_graph_ql->get('override');
    if (!empty($override['enable']) && $override['enable'] == TRUE && !empty($override[$key])) {
      return $override[$key];
    }
    return $this->previewGraphQlBackend->getConfig()->get($key);
  }

  /**
   * Return an data set into cache bin. I.E: form_state, gql,callback_url.
   *
   * @param string $cache_data_key
   *   The key searched into cache. I.E: form_state, gql,callback_url.
   *
   * @return array|string
   *   IF array, contains error (key 'error'), else return data searched.
   */
  public function getCacheDataValue($cache_data_key) {

    $cid = $this->previewGraphQlBackend->getCid();
    if (empty($cid)) {
      return FALSE;
    }
    $cache = $this->previewGraphQlBackend->getCacheBackend()->get($cid);
    if (empty($cache->data[$cache_data_key])) {
      $message = $this->t('Invalid key request, %key is empty.', ['%key' => $cache_data_key]);
      $this->previewGraphQlBackend->getMessenger()
        ->addError($message);
      $this->previewGraphQlBackend->getLogger()
        ->error($message);
      return FALSE;
    }
    return $cache->data[$cache_data_key];
  }

  /**
   * Set into cache data.
   *
   * @param string $cid
   *   The cache id where to save data.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The data form_state of entity form.
   * @param array $graph_ql_resut
   *   The graphql data result.
   * @param string $url_callback
   *   The url of entity form.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The old entity.
   *
   * @return array|bool
   *   IF array, contains error (key 'error'), else return true.
   */
  public function setEntityCache($cid, FormStateInterface $form_state, array $graph_ql_resut, $url_callback, EntityInterface $entity) {
    if (empty($cid) || empty($form_state) || empty($graph_ql_resut) || empty($url_callback) || empty($entity)) {
      $message = $this->t("Can't set cache some data are empty.");
      $this->previewGraphQlBackend->getMessenger()
        ->addError($message);
      $this->previewGraphQlBackend->getLogger()
        ->error($message);
      return FALSE;
    }
    $cache = [];
    if (!empty($entity->getCacheTags())) {
      $cache = $entity->getCacheTags();
    }
    // Add our cache tag.
    $cache[] = $this->getTagName($entity);
    $this->previewGraphQlBackend->getCacheBackend()->set($cid, [
      'gql' => $graph_ql_resut,
      'form_state' => $form_state,
      'callback_url' => $url_callback,
    ], $this->dateTime->getRequestTime() + self::DEFAULT_TIME_CACHE, $cache);
    return TRUE;
  }

  /**
   * Return the preview graphql tag name for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to build the tag name.
   *
   * @return string
   *   The tag name.
   */
  public function getTagName(EntityInterface $entity) {
    return self::PREFIX_CACHE . $entity->getEntityTypeId() . ':' . $entity->uuid();
  }

  /**
   * Delete the cache of entity.
   *
   * @param string|null $cid
   *   The cache id to delete.
   *
   * @return array|string|null
   *   IF array, contains error (key 'error').
   */
  public function deleteEntityCache($cid = NULL) {
    if (empty($cid)) {
      $cid = $this->previewGraphQlBackend->getCid();
    }
    if (empty($cid)) {
      return FALSE;
    }
    $this->previewGraphQlBackend->getCacheBackend()->delete($cid);
    return TRUE;
  }

  /**
   * Send data to front (do POST and redirection).
   *
   * @param \Drupal\graphql\GraphQL\Execution\QueryResult $result
   *   The graphQL result of preview.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity doing the preview.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current data in form of preview.
   *
   * @return bool
   *   True if all has been completed.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function sendDataFront(QueryResult $result, EntityInterface $entity, FormStateInterface $form_state) {

    // Clear old cache for this entity.
    $this->previewGraphQlBackend->getCacheTagInvalidator()
      ->invalidateTags([$this->getTagName($entity)]);
    $url_front = $this->getUrlFrontByEntity($entity);
    if ($url_front == FALSE) {
      return FALSE;
    }
    $uuid = $this->uuid->generate();
    $callback_url = $this->previewGraphQlBackend->getRequest()
      ->getUri();
    // Save in cache the entity and graphql results.
    if (!$this->setEntityCache($uuid, $form_state, $result->data, $callback_url, $entity)) {
      return FALSE;
    }
    // No carrier callback set.
    $carrier_callback = $this->previewGraphQlBackend->getConfig()
      ->get('carrier_callback');
    if (empty($carrier_callback)) {
      return FALSE;
    }

    // Get the value to transmit (token or cache id).
    $event = new GenerateKeyEvent($carrier_callback, $uuid);
    $this->previewGraphQlBackend->getEventDispatcher()
      ->dispatch(GenerateKeyEvent::EVENT_NAME, $event);
    $url_parameter_value = $event->getValueKey();
    $method = $this->previewGraphQlBackend->getConfig()->get('method');
    // POST data method.
    if ($method == 'post') {

      $post_url_key = $this->previewGraphQlBackend->getConfig()
        ->get('post_url_key');
      // Send http post to front.
      try {

        $request = $this->httpClient->post($url_front, [
          'json' => [
            'data' => $result->data,
            'key_redirect_name' => $post_url_key,
            'callback_url' => $callback_url,
          ],
        ]);
      }
      catch (Exception $e) {
        $this->previewGraphQlBackend->getMessenger()
          ->addError($e->getMessage());
        $this->previewGraphQlBackend->getLogger()
          ->error($e->getMessage());
        return FALSE;
      }

      $response = json_decode($request->getBody());
      // Fetch the callback post url.
      if (empty($response[$post_url_key])) {
        $message = $this->t("No callback url for the preview found into POST response.");
        $this->previewGraphQlBackend->getMessenger()
          ->addError($message);
        $this->previewGraphQlBackend->getLogger()
          ->error($message);
        return FALSE;
      }
      $url_front = $response[$post_url_key];
      if (UrlHelper::isValid($url_front) == FALSE) {
        $message = $this->t("Callback url for the preview found into POST response is not an valid url.");
        $this->previewGraphQlBackend->getMessenger()
          ->addError($message);
        $this->previewGraphQlBackend->getLogger()
          ->error($message);
        return FALSE;
      }

    }

    $url_parameter = $this->getUrlParameter($url_parameter_value);
    $response = new TrustedRedirectResponse($url_front . $url_parameter);
    // Remove destination.
    $this->previewGraphQlBackend->getRequest()->query->remove('destination');
    $metadata = $response->getCacheableMetadata();
    $metadata->setCacheMaxAge(0);
    $metadata->addCacheableDependency($result);
    // Event alter response before redirect.
    $event = new RedirectEvent($carrier_callback, $response, $url_parameter_value);
    $this->previewGraphQlBackend->getEventDispatcher()
      ->dispatch(RedirectEvent::EVENT_NAME, $event);
    $response = $event->getResponse();
    $form_state->disableRedirect();
    $form_state->setResponse($response);
    return TRUE;
  }

  /**
   * Build the front url by entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity doing the preview.
   *
   * @return bool|string
   *   False if error, elee the front url.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUrlFrontByEntity(EntityInterface $entity) {
    // Get url front.
    $url_front = $this->previewGraphQlBackend->getConfig()->get('url_front');
    if (empty($url_front)) {
      $message = $this->t('No front url set in global settings');
      $this->previewGraphQlBackend->getMessenger()->addError($message);
      $this->previewGraphQlBackend->getLogger()->error($message);
      return FALSE;
    }
    // Get the uri.
    $uri_front = $this->getSettingByEntity('uri_front', $entity);
    if (substr($uri_front, 0, 1) != '/') {
      $url_front .= '/';
    }
    $url_front .= $uri_front;
    return $url_front;
  }

  /**
   * Return the url parameter in case of default carrier key.
   *
   * @param string $cache_id
   *   The cache id of preview.
   *
   * @return string
   *   The url parameter.
   */
  public function getUrlParameter($cache_id) {
    if ($this->previewGraphQlBackend->getConfig()->get('carrier_callback') == 'key') {
      return '?' . $this->previewGraphQlBackend->getConfig()->get('carrier_callback_key') . '=' . $cache_id;
    }
    return '?' . $this->previewGraphQlBackend->getConfig()->get('carrier_callback') . '=' . $cache_id;
  }

}
