<?php

namespace Drupal\preview_graphql\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Preview graph QL entity entities.
 */
interface PreviewGraphQLInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
