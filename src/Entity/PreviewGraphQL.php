<?php

namespace Drupal\preview_graphql\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Preview graph QL entity entity.
 *
 * @ConfigEntityType(
 *   id = "preview_graphql",
 *   label = @Translation("Preview graph QL entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\preview_graphql\PreviewGraphQLListBuilder",
 *     "form" = {
 *       "add" = "Drupal\preview_graphql\Form\PreviewGraphQLForm",
 *       "edit" = "Drupal\preview_graphql\Form\PreviewGraphQLForm",
 *       "delete" = "Drupal\preview_graphql\Form\PreviewGraphQLDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\preview_graphql\PreviewGraphQLHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "preview_graphql",
 *   admin_permission = "access administration preview_graphql",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "entity_type" = "entity_type",
 *     "bundle" = "bundle"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/preview-graphql/{preview_graphql}",
 *     "add-form" = "/admin/structure/preview-graphql/add",
 *     "edit-form" = "/admin/structure/preview-graphql/{preview_graphql}/edit",
 *     "delete-form" = "/admin/structure/preview-graphql/{preview_graphql}/delete",
 *     "collection" = "/admin/structure/preview-graphql"
 *   }
 * )
 */
class PreviewGraphQL extends ConfigEntityBase implements PreviewGraphQLInterface {

  /**
   * The Preview graph QL entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Preview graph QL entity type.
   *
   * @var string
   */
  protected $entity_type;
  /**
   * The Preview graph QL bundle.
   *
   * @var array
   */
  protected $bundle;

  /**
   * The Preview graph QL deriver type.
   *
   * @var bool
   */
  protected $disable_preview_btn;
  /**
   * The Preview graph QL deriver type.
   *
   * @var string
   */
  protected $deriver;

  /**
   * Settings override.
   *
   * @var array
   */
  protected $override;

}
