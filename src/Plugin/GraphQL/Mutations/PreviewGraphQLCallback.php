<?php

namespace Drupal\preview_graphql\Plugin\GraphQL\Mutations;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Mutations\MutationPluginBase;
use Drupal\graphql_core\GraphQL\EntityCrudOutputWrapper;
use Drupal\preview_graphql\Services\PreviewGraphQlManager;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Simple mutation for creating a new article node.
 *
 * @GraphQLMutation(
 *   id = "preview_graphql_callback",
 *   secure = true,
 *   name = "previewGraphQLCallback",
 *   type = "EntityCrudOutput",
 *   arguments = {
 *      "input" = "previewGraphQLInput"
 *   }
 * )
 */
class PreviewGraphQLCallback extends MutationPluginBase implements ContainerFactoryPluginInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The preview graphQL manager.
   *
   * @var \Drupal\preview_graphql\Services\PreviewGraphQlManager
   */
  protected $previewGraphQLManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('preview_graphql.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, EntityTypeManagerInterface $entityTypeManager, RendererInterface $renderer, PreviewGraphQlManager $preview_graphql_manager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->previewGraphQLManager = $preview_graphql_manager;;
    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition() {
    $definition = $this->getPluginDefinition();

    return [
      'type' => $this->buildType($definition),
      'description' => $this->buildDescription($definition),
      'args' => $this->buildArguments($definition),
      'deprecationReason' => $this->buildDeprecationReason($definition),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function resolve($value, array $args, ResolveContext $context, ResolveInfo $info) {

    return $this->renderer->executeInRenderContext(new RenderContext(), function () use ($value, $args, $context, $info) {

      $form_state = $this->previewGraphQLManager->getCacheDataValue('form_state');
      if (empty($form_state) || !($form_state instanceof FormStateInterface)) {
        $error = !empty($form_state['error']) ? $form_state['error'] : t('Cache form state error');
        return new EntityCrudOutputWrapper(NULL, NULL, [
          $error,
        ]);
      }
      $entity = $form_state->getFormObject()->getEntity();
      if (is_array($entity) && !empty($entity['error'])) {
        return new EntityCrudOutputWrapper(NULL, NULL, [
          $entity['error'],
        ]);
      }

      // If cancel we delete of cache.
      if ($args['operation'] == 'cancel') {
        $result = $this->previewGraphQLManager->deleteEntityCache();
        if (!empty($result['error'])) {
          throw new Error(sprintf('%s', $result['error']));
        }
        return new EntityCrudOutputWrapper($entity, NULL, [
          $this->t('Saved has been cancel'),
        ]);
      }

      try {
        // Save entity.
        $entity->save();
        $result = $this->previewGraphQLManager->deleteEntityCache();
        if (!empty($result['error'])) {
          throw new Error(sprintf('%s', $result['error']));
        }
      }
      catch (EntityStorageException $exception) {
        return new EntityCrudOutputWrapper($entity, NULL, [
          $this->t('Entity deletion failed with exception: @exception.', [
            '@exception' => $exception->getMessage(),
          ]),
        ]);
      }

      return new EntityCrudOutputWrapper($entity);
    });

  }

}
