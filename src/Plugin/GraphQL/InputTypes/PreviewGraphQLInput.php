<?php

namespace Drupal\preview_graphql\Plugin\GraphQL\InputTypes;

use Drupal\graphql\Plugin\GraphQL\InputTypes\InputTypePluginBase;

/**
 * The input type for article mutations.
 *
 * @GraphQLInputType(
 *   id = "preview_graphql_input",
 *   name = "previewGraphQLInput",
 *   fields = {
 *     "operation" = "String",
 *   }
 * )
 */
class PreviewGraphQLInput extends InputTypePluginBase {

}
