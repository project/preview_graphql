<?php

namespace Drupal\preview_graphql\Plugin\GraphQL\Fields;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Preview graphQL field deriver.
 *
 * @GraphQLField(
 *   id = "preview_graphql_field_deriver",
 *   secure = true,
 *   name = "previewGraphQLFieldDeriver",
 *   deriver =
 *   "Drupal\preview_graphql\Plugin\Deriver\Fields\PreviewGraphQLDeriver"
 * )
 */
class PreviewGraphQLFieldDeriver extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The temp store manager.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, PrivateTempStoreFactory $temp_store_factory) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->tempStoreFactory = $temp_store_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if (!empty($args['entity_type']) && !empty($args['uuid'])) {
      $temp_store = $this->tempStoreFactory->get('preview_graphql');
      $form_state = $temp_store->get($args['entity_type'] . '_' . $args['uuid']);
      if (empty($form_state)) {
        throw new Error(sprintf("Preview temps store error. Can't load entity,temp store id doesn't exist."));
      }

      $entity = $form_state->getFormObject()->getEntity();
      if (empty($entity)) {
        throw new Error(sprintf("Preview temps store error. Can't load entity, form state has been corrupt."));
      }
      yield $entity;
    }
    else {
      throw new Error(sprintf('Preview temps store error. Arguments are empty'));
    }
  }

}
