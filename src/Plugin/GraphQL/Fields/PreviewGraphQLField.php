<?php

namespace Drupal\preview_graphql\Plugin\GraphQL\Fields;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\preview_graphql\Event\ContextResolverCacheEvent;
use Drupal\preview_graphql\Services\PreviewGraphQlManager;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Preview graphQL field. Return the data in cache.
 *
 * @GraphQLField(
 *   id = "preview_graphql_field",
 *   secure = true,
 *   name = "previewGraphQLField",
 *   type = "any!"
 * )
 */
class PreviewGraphQLField extends FieldPluginBase implements ContainerFactoryPluginInterface {


  /**
   * A preview graphql manager service.
   *
   * @var \Drupal\preview_graphql\Services\PreviewGraphQlManager
   */
  protected $previewGraphQLManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('preview_graphql.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, PreviewGraphQlManager $preview_graphql_manager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->previewGraphQLManager = $preview_graphql_manager;

  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {

    $carrier_callback = $this->previewGraphQLManager->getPreviewGraphQlBackend()
      ->getConfig()
      ->get('carrier_callback');
    if (empty($carrier_callback)) {
      throw new Error(sprintf('%s', t('Missing carrier callback settings')));
    }
    $context->addCacheContexts(['url.query_args']);

    // Alter cache.
    $event = new ContextResolverCacheEvent($carrier_callback, $context);
    $this->previewGraphQLManager->getPreviewGraphQlBackend()
      ->getEventDispatcher()
      ->dispatch(ContextResolverCacheEvent::EVENT_NAME, $event);
    $result = $this->previewGraphQLManager->getCacheDataValue('gql');
    if (is_array($result) && !empty($result['error'])) {
      throw new Error(sprintf('%s', $result['error']));
    }
    yield $result;
  }

}
