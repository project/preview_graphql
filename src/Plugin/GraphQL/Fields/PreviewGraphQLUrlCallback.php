<?php

namespace Drupal\preview_graphql\Plugin\GraphQL\Fields;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\preview_graphql\Services\PreviewGraphQlManager;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Get the url callback backend. The current url of entity form.
 *
 * @GraphQLField(
 *   id = "preview_graphql_url_callback",
 *   secure = true,
 *   name = "previewGraphQLUrlCallback",
 *   type = "String!",
 *   response_cache_contexts = {"headers:Authorization"}
 * )
 */
class PreviewGraphQLUrlCallback extends FieldPluginBase implements ContainerFactoryPluginInterface {


  protected $previewGraphQLManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('preview_graphql.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, PreviewGraphQlManager $preview_graphql_manager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->previewGraphQLManager = $preview_graphql_manager;

  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $result = $this->previewGraphQLManager->getCacheDataValue('callback_url');
    if (is_array($result) && !empty($result['error'])) {
      throw new Error(sprintf('%s', $result['error']));
    }

    yield $result;
  }

}
