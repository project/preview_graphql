<?php

namespace Drupal\preview_graphql\Plugin\Deriver\Fields;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\graphql\Utility\StringHelper;
use Drupal\preview_graphql\Services\PreviewGraphQlManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derives content entity fields as GraphQL fields.
 */
class PreviewGraphQLDeriver extends DeriverBase implements ContainerDeriverInterface {


  /**
   * An instance of preview graphql manager.
   *
   * @var \Drupal\preview_graphql\Services\PreviewGraphQlManager
   */
  protected $previewGraphQLManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(PreviewGraphQlManager $preview_graphql_manager) {
    $this->previewGraphQLManager = $preview_graphql_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $basePluginId) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('preview_graphql.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    $deriver_type = $this->previewGraphQLManager->getDeriverType();
    foreach ($deriver_type['entity_type'] as $entity_type) {

      $derived_id = 'preview_graphql_' . $entity_type;
      $this->derivatives[$derived_id] = [
        'name' => StringHelper::propCase('Preview_' . $entity_type),
        'type' => StringHelper::camelCase($entity_type),
        'arguments' => ['entity_type' => 'String!', 'uuid' => 'String!'],
        'response_cache_contexts' => ['headers:Authorization'],
      ] + $basePluginDefinition;
    }

    foreach ($deriver_type['bundle'] as $entity_type => $bundles) {
      foreach ($bundles as $bundle) {
        $derived_id = 'preview_graphql_' . $entity_type . '_' . $bundle;
        $this->derivatives[$derived_id] = [
          'name' => StringHelper::propCase('Preview_' . $entity_type . ' ' . $bundle),
          'type' => StringHelper::camelCase($entity_type . ' ' . $bundle),
          'arguments' => ['entity_type' => 'String!', 'uuid' => 'String!'],
          'response_cache_contexts' => ['headers:Authorization'],
        ] + $basePluginDefinition;
      }
    }
    return parent::getDerivativeDefinitions($basePluginDefinition);
  }

}
