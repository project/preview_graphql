<?php

namespace Drupal\preview_graphql_jwt\Services;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\JsonWebToken\JsonWebToken;
use Drupal\jwt\Transcoder\JwtTranscoderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PreviewGraphQLBackend.
 *
 * @package Drupal\preview_graphql\Services
 */
class PreviewGraphQlJwtManager {

  use StringTranslationTrait;

  /**
   * An JWT token.
   *
   * @var string
   */
  protected $jwt;


  /**
   * The cache id of preview.
   *
   * @var string
   */
  protected $cid;

  /**
   * A transcodeer JWT service.
   *
   * @var \Drupal\jwt\Transcoder\JwtTranscoderInterface
   */
  protected $transcoderJwt;


  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * A messenger instance.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * A entity logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;


  /**
   * The request instance.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * Key to store the cache id insie JWY.
   */
  const CLAIM_ID = 'preview_gql_cid';

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack, JwtTranscoderInterface $transcoder_jwt, EventDispatcherInterface $event_dispatcher, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger_factory, TranslationInterface $string_translation) {
    $this->request = $request_stack->getCurrentRequest();
    $this->transcoderJwt = $transcoder_jwt;
    $this->eventDispatcher = $event_dispatcher;
    $this->messenger = $messenger;
    $this->logger = $logger_factory->get('preview_graphql');
    $this->stringTranslation = $string_translation;
  }

  /**
   * Return the request manager.
   *
   * @return \Symfony\Component\HttpFoundation\Request|null
   *   An instance of request manager.
   */
  public function getRequest() {
    return $this->request;
  }

  /**
   * Return the messenger service.
   *
   * @return \Drupal\Core\Messenger\MessengerInterface
   *   The messenger service.
   */
  public function getMessenger() {
    return $this->messenger;
  }

  /**
   * Return the logger service.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   The logger service.
   */
  public function getLogger() {
    return $this->logger;
  }

  /**
   * Get the JWT token into current request.
   *
   * @return string|bool
   *   The JWT decoded.False if error.
   */
  public function getJwt() {
    if (!empty($this->jwt)) {
      return $this->jwt;
    }
    $auth_header = $this->request->headers->get('Authorization');
    $matches = [];
    if (!preg_match('/^Bearer (.*)/', $auth_header, $matches)) {
      return FALSE;
    }
    return $this->setJwt($matches[1]);
  }

  /**
   * Set the JWT into class.
   *
   * @param string $jwt
   *   The JWT encoded to decode and set.
   *
   * @return string|bool
   *   The JWT decoded.False if error.
   */
  protected function setJwt($jwt) {
    $jwt_decode = $this->transcoderJwt->decode($jwt);
    if (empty($jwt_decode)) {
      $message = $this->t('Invalid jwt.');
      $this->messenger->addError($message);
      $this->logger->error($message);
      return FALSE;
    }
    $this->jwt = $jwt_decode;
    return $jwt_decode;
  }

  /**
   * Generate new JWT token and add cid into payload.
   *
   * @param string $claim
   *   The claim to add, i.e the cid where the entity is stored.
   *
   * @return string
   *   The JWT token generate with the claim inside.
   */
  public function generateJwt($claim) {
    // Create new jwt.
    $event = new JwtAuthGenerateEvent(new JsonWebToken());
    /* @var  \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher */
    $this->eventDispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
    $jwt = $event->getToken();
    // Add into payload the cid.
    $event->addClaim(self::CLAIM_ID, $claim);
    /* @var  $transcoder \Drupal\jwt\Transcoder\JwtTranscoderInterface */
    return $this->transcoderJwt->encode($jwt);
  }

  /**
   * Return the cid into the JWT token payload.
   *
   * @return array|string
   *   The cid inside the jwt
   */
  public function getCidByJwt() {
    if (!empty($this->cid)) {
      return $this->cid;
    }
    $jwt = $this->getJwt();
    if (is_array($jwt) || empty($jwt)) {
      return $jwt;
    }
    $cid = $jwt->getClaim(self::CLAIM_ID);
    if (empty($cid)) {

      $message = $this->t('Invalid jwt, cid is empty.');
      $this->messenger->addError($message);
      $this->logger->error($message);
      return FALSE;
    }
    $this->cid = $cid;
    return $cid;
  }

}
