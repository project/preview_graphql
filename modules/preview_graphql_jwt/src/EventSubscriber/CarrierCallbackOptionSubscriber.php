<?php

namespace Drupal\preview_graphql_jwt\EventSubscriber;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\preview_graphql\Event\CarrierCallbackOptionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CarrierCallbackOptionSubscriber. Subscribe to options carrier.
 *
 * @package Drupal\preview_graphql_jwt\EventSubscriber
 */
class CarrierCallbackOptionSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[CarrierCallbackOptionEvent::EVENT_NAME] = 'getCarrierCallbackOption';
    return $events;
  }

  /**
   * Add JWT option to carrier settings.
   *
   * @param \Drupal\preview_graphql\Event\CarrierCallbackOptionEvent $event
   *   The carrier callback option event.
   */
  public function getCarrierCallbackOption(CarrierCallbackOptionEvent $event) {

    $event->addOption(['jwt' => $this->t('Use JWT token to transmit data.')]);
  }

}
