<?php

namespace Drupal\preview_graphql_jwt\EventSubscriber;

use Drupal\preview_graphql\Event\ContextResolverCacheEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CarrierCallbackOptionSubscriber. Subscribe to context resolver cache.
 *
 * @package Drupal\preview_graphql_jwt\EventSubscriber
 */
class ContextResolverCacheSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ContextResolverCacheEvent::EVENT_NAME] = 'alterContext';
    return $events;
  }

  /**
   * Alter the cache context resolver to add cache context on headers.
   *
   * @param \Drupal\preview_graphql\Event\ContextResolverCacheEvent $event
   *   The carrier callback option event.
   */
  public function alterContext(ContextResolverCacheEvent $event) {
    if ($event->carrierCallback == 'jwt') {
      $event->getContext()->addCacheContexts(['headers:Authorization']);
    }
  }

}
