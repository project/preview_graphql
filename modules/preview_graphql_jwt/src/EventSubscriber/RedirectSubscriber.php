<?php

namespace Drupal\preview_graphql_jwt\EventSubscriber;

use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\preview_graphql\Event\RedirectEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class TermPageAccessSubscriber. Subscribe to generate key event.
 *
 * @package Drupal\term_page_access
 */
class RedirectSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RedirectEvent::EVENT_NAME] = 'redirect';
    return $events;
  }

  /**
   * Alter the redirection to front, to add JWT inside headers.
   *
   * @param \Drupal\preview_graphql\Event\RedirectEvent $event
   *   The redirect event.
   */
  public function redirect(RedirectEvent $event) {
    if ($event->carrierCallback == 'jwt') {
      $response = $event->getResponse();
      // Set the jwt token into headers.
      $url = $url = strtok($response->getTargetUrl(), "?");
      $new_response = new TrustedRedirectResponse($url, 302, ['Authorization' => 'Bearer ' . $event->urlParameterValue]);
      $event->setResponse($new_response);
    }
  }

}
