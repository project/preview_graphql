<?php

namespace Drupal\preview_graphql_jwt\EventSubscriber;

use Drupal\preview_graphql\Event\GenerateKeyEvent;
use Drupal\preview_graphql_jwt\Services\PreviewGraphQlJwtManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class TermPageAccessSubscriber. Subscribe to generate key event.
 *
 * @package Drupal\term_page_access
 */
class GenerateKeySubscriber implements EventSubscriberInterface {

  /**
   * The preview graphql JWT manager.
   *
   * @var \Drupal\preview_graphql_jwt\Services\PreviewGraphQlJwtManager
   */
  protected $previewGraphQlJwtManager;

  /**
   * GenerateKeySubscriber constructor.
   *
   * @param \Drupal\preview_graphql_jwt\Services\PreviewGraphQlJwtManager $preview_graphql_jwt_manager
   *   The preview graphql JWT manager.
   */
  public function __construct(PreviewGraphQlJwtManager $preview_graphql_jwt_manager) {
    $this->previewGraphQlJwtManager = $preview_graphql_jwt_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[GenerateKeyEvent::EVENT_NAME] = 'generateKey';
    return $events;
  }

  /**
   * Generate the JWT token value with the cid inside.
   *
   * @param \Drupal\preview_graphql\Event\GenerateKeyEvent $event
   *   The generate key event.
   */
  public function generateKey(GenerateKeyEvent $event) {

    if ($event->carrierCallback == 'jwt' && !empty($event->cid)) {
      $value = $this->previewGraphQlJwtManager->generateJwt($event->cid);
      if (!empty($value)) {
        $event->setValueKey($value);
      }
    }
  }

}
