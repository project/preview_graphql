<?php

namespace Drupal\preview_graphql_jwt\EventSubscriber;

use Drupal\preview_graphql\Event\GetCidEvent;
use Drupal\preview_graphql_jwt\Services\PreviewGraphQlJwtManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class TermPageAccessSubscriber. Subscribe to get cid event.
 *
 * @package Drupal\term_page_access
 */
class GetCidSubscriber implements EventSubscriberInterface {

  /**
   * The preview graphql JWT manager.
   *
   * @var \Drupal\preview_graphql_jwt\Services\PreviewGraphQlJwtManager
   */
  protected $previewGraphQlJwtManager;

  /**
   * GetCidSubscriber constructor.
   *
   * @param \Drupal\preview_graphql_jwt\Services\PreviewGraphQlJwtManager $preview_graphql_jwt_manager
   *   The preview graphql JWT manager.
   */
  public function __construct(PreviewGraphQlJwtManager $preview_graphql_jwt_manager) {
    $this->previewGraphQlJwtManager = $preview_graphql_jwt_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[GetCidEvent::EVENT_NAME] = 'getCid';
    return $events;
  }

  /**
   * Return the cid inside JWT.
   *
   * @param \Drupal\preview_graphql\Event\GetCidEvent $event
   *   The get cid event.
   */
  public function getCid(GetCidEvent $event) {

    if ($event->carrierCallback == 'jwt') {
      $cid = $this->previewGraphQlJwtManager->getCidByJwt();
      if (!empty($cid)) {
        $event->setCid($cid);
      }
    }
  }

}
