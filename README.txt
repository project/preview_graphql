CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Developers
 * Maintainers

INTRODUCTION
------------
This module provide a preview for entity when you are decoupled.


REQUIREMENTS
------------
GraphQL

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. For further
information, see:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules

CONFIGURATION
-------------

Configure global settings :
/admin/config/services/preview-gql/settings

Add specific settings for entity :
/admin/structure/preview-graphql

For more information :
https://www.drupal.org/docs/8/modules/preview-graphql

DEVELOPERS
----------

@todo explain event

MAINTAINERS
-----------
Current maintainers:
  * Thomas MUSA - https://www.drupal.org/u/musathomas
